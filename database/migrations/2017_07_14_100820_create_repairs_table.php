<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('repairs', function (Blueprint $table) {

            $table->increments('id', true);
            $table->integer('client_id')->unsigned();
            $table->integer('repairtype_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('problem', 350);
            $table->string('service', 180);
            $table->string('comment', 200);
            $table->string('revisit', 45);
            $table->string('client_signature', 200);
            $table->string('user_signature', 200);
            $table->string('observation', 300)->nullable();
            $table->softDeletes();
            $table->timestamps();


        });

        Schema::table('repairs', function($table) {
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('repairtype_id')->references('id')->on('repair_types');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repairs');
    }
}
