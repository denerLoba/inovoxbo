<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinhaRepairsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linha_repairs_products', function (Blueprint $table) {
            $table->integer('repair_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->decimal('quantity', 5, 2);
            $table->decimal('price', 5, 2);
            $table->string('description', 150)->nullable();
            $table->softDeletes();
            $table->timestamps();

        });

        Schema::table('linha_repairs_products', function (Blueprint $table) {
            $table->foreign('repair_id')->references('id')->on('repairs');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linha_repairs_products');
    }
}
