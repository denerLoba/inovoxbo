<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipaments', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('name', 150);
            $table->string('brand', 150);
            $table->string('code', 45)->nullable();
            $table->integer('repair_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();


        });

        Schema::table('equipaments', function (Blueprint $table) {
            $table->foreign('repair_id')->references('id')->on('repairs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipaments');
    }
}
